CC=gcc
CFLAGS=-I. -std=c99 -Wall -Werror -D_POSIX_C_SOURCE=199309L
DEPS = circular_buffer.h bs_exam2_task1.h utils.h
OBJ = $(DEPS:.h=.o)
LDLIBS = -lpthread

.PHONY: all clean
all: circular_buffer_test main

circular_buffer_test: circular_buffer_test.o $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
	
main: main.o $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDLIBS)

clean:
	rm -f *.o circular_buffer_test main
	
