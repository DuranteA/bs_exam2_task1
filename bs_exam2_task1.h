#pragma once

#include <pthread.h>

#include "circular_buffer.h"

// NOTE: all buffer operations must be synchronized

// creates a new thread, which continuously tries to read work_packages from "buffer"
// it should write all packages it receives to a new file with the name indicated by "filename"
// if "include_timestamps" is set, then the timestep value should precede each entry wirtten to the file, otherwise it should be ignored
pthread_t spawn_consumer_thread(circ_buffer* buffer, bool include_timestamps, const char* filename);

// creates a new thread which generates random work_packages and writes them to "buffer"
// after each successfully written message, the thread sleeps for "sleep_milliseconds"
pthread_t spawn_producer_thread(circ_buffer* buffer, unsigned sleep_milliseconds);

// stops the execution of the consumer or producer thread "t"
// the system should ensure that no packages are lost
// -> consumers must finish their current package
// -> if there is only one consumer thread remaining, it must continue working until there are no packages left in the buffer
void stop_thread(pthread_t t);
