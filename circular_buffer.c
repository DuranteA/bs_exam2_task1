#include "circular_buffer.h"

void circ_buffer_init(circ_buffer* buffer) {
	memset(buffer, 0, sizeof(circ_buffer));
	buffer->cur_after_end = 1;
}

bool circ_buffer_is_full(circ_buffer* buffer) {
	return buffer->cur_after_end == buffer->cur_start;
}

bool circ_buffer_is_empty(circ_buffer* buffer) {
	return buffer->cur_after_end == (buffer->cur_start + 1) % NUM_ENTRIES;
}

bool circ_buffer_insert(circ_buffer* buffer, work_package* in_package) {
	if(circ_buffer_is_full(buffer)) return false;
	int prev_end = buffer->cur_after_end - 1;
	if(prev_end < 0) prev_end = NUM_ENTRIES - 1;
	buffer->cur_after_end = (buffer->cur_after_end + 1) % NUM_ENTRIES;
	memcpy(&buffer->data[prev_end], in_package, sizeof(work_package));
	return true;
}

bool circ_buffer_take(circ_buffer* buffer, work_package* out_package) {
	if(circ_buffer_is_empty(buffer)) return false;
	unsigned prev_start = buffer->cur_start;
	buffer->cur_start = (buffer->cur_start + 1) % NUM_ENTRIES;
	memcpy(out_package, &buffer->data[prev_start], sizeof(work_package));
	return true;
}
