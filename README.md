# Operating Systems Lab 2017 - Exam 2 #

In this exercise, a threaded multi-producer multi-consumer system needs to be implemented.

The consumers and producers communicate over a single shared circular buffer, the sequential implementation of which is provided.

### Structure of this repository ###

These files are already available in the repository:

* a `Makefile` for building the programs
* `utils.[h/c]` containing a utility function for millisecond-based sleeping
* `circular_buffer.[h/c]` which contains the implementation of the circular buffer
* `circular_buffer_test.c` is a test program for checking the basic functionality of the circular buffer
* `bs_exam2_task1.[h/c]` where the majority of the implementation for this exercise should take place
* `main.c` which implements a test program that is already pre-defined

### Your task ###

Implement the functions `spawn_consumer_thread`, `spawn_producer_thread` and `stop_thread` as outlined in
`bs_exam2_task1.h`. For details, refer to the comments provided with each function declaration. 
Also make sure that all uses of the circular buffer are protected from concurrent access,
or change the circular buffer implementation itself to be thread-safe.


Take note that:

* You are **not** allowed to change the interface of any of these functions or the main program
* You **are** allowed to add additional data members to the `circ_buffer` structure as required for synchronization
* Once you have implemented the basics, take care to fulfill the specific requirements outlined for `stop_thread`

### The big picture ###

The overall system should look like this:

![Illustration](images/illustration.png)

Multiple producer threads insert new work into the circular buffer by using `circ_buffer_insert`.
Multiple consumer threads try to take work from the buffer using `circ_buffer_take`, and, if successful, 
write the resulting data packet to a file according to how they were configured in the call to `spawn_consumer_thread`.