#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#define NUM_ENTRIES 16
#define ENTRY_MAXLEN 40

typedef struct work_package {
	unsigned timestamp;
	const char entry[ENTRY_MAXLEN];
} work_package;

typedef struct circ_buffer {
	work_package data[NUM_ENTRIES];
	unsigned cur_start, cur_after_end;
	/// YOUR DATA HERE
} circ_buffer;

void circ_buffer_init(circ_buffer* buffer);

bool circ_buffer_is_full(circ_buffer* buffer);
bool circ_buffer_is_empty(circ_buffer* buffer);

bool circ_buffer_insert(circ_buffer* buffer, work_package* in_package);
bool circ_buffer_take(circ_buffer* buffer, work_package* out_package);
