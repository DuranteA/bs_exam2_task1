#include "bs_exam2_task1.h"

#include "utils.h"

int main() {

	circ_buffer buffer;

	pthread_t consumer_one = spawn_consumer_thread(&buffer, false, "one.txt");
	pthread_t consumer_two = spawn_consumer_thread(&buffer, true, "two.txt");
	pthread_t consumer_three = spawn_consumer_thread(&buffer, false, "three.txt");
	pthread_t consumer_four = spawn_consumer_thread(&buffer, true, "four.txt");

	pthread_t producer_one = spawn_producer_thread(&buffer, 100);
	pthread_t producer_two = spawn_producer_thread(&buffer, 50);

	millisleep(200);
	stop_thread(consumer_one);
	millisleep(200);
	stop_thread(producer_one);
	stop_thread(consumer_two);
	millisleep(100);
	stop_thread(producer_two);
	stop_thread(consumer_three);
	millisleep(100);
	stop_thread(consumer_four);

	pthread_t threads[6] = { consumer_one, consumer_two, consumer_three, consumer_four, producer_one, producer_two };
	for(int i = 0; i < 6; ++i) {
		pthread_join(threads[i], NULL);
	}
}
