#include "circular_buffer.h"

#include <stdio.h>

#define TEST_EQ(_v, _e) if((_v) != (_e)) printf("Test failed: %s != %s in line %d\n", #_v, #_e, __LINE__);

int main() {

	circ_buffer testbuffer;
	circ_buffer *ptb = &testbuffer;
	circ_buffer_init(ptb);

	TEST_EQ(circ_buffer_is_empty(ptb), true);
	TEST_EQ(circ_buffer_is_full(ptb), false);

	// test insertion
	{
		work_package test_wp = { 42, "Test" };
		TEST_EQ(circ_buffer_insert(ptb, &test_wp), true);

		TEST_EQ(circ_buffer_is_empty(ptb), false);
		TEST_EQ(circ_buffer_is_full(ptb), false);
	}

	// test removal
	{
		work_package taken_wp;
		TEST_EQ(circ_buffer_take(ptb, &taken_wp), true);

		TEST_EQ(circ_buffer_is_empty(ptb), true);
		TEST_EQ(circ_buffer_is_full(ptb), false);

		TEST_EQ(taken_wp.timestamp, 42);
		TEST_EQ(strcmp(taken_wp.entry, "Test"), 0);
	}

	// test multiple insertion/removal
	{
		work_package wps[3] = { { 1, "A" }, { 2, "B" }, { 2, "C" } };

		for(int i = 0; i < 3; ++i) {
			TEST_EQ(circ_buffer_insert(ptb, &wps[i]), true);
		}

		TEST_EQ(circ_buffer_is_empty(ptb), false);
		TEST_EQ(circ_buffer_is_full(ptb), false);

		for(int i = 0; i < 3; ++i) {
			work_package taken_wp;
			TEST_EQ(circ_buffer_take(ptb, &taken_wp), true);

			TEST_EQ(taken_wp.timestamp, wps[i].timestamp);
			TEST_EQ(strcmp(taken_wp.entry, wps[i].entry), 0);
		}

		TEST_EQ(circ_buffer_is_empty(ptb), true);
		TEST_EQ(circ_buffer_is_full(ptb), false);
	}

	// test filling up completely and removing again
	{
		// test that removal fails (because empty)
		TEST_EQ(circ_buffer_take(ptb, NULL), false);

		for(int i = 0; i < NUM_ENTRIES-1; ++i) {
			work_package insert_wp = {i, ""};
			TEST_EQ(circ_buffer_insert(ptb, &insert_wp), true);
		}

		TEST_EQ(circ_buffer_is_empty(ptb), false);
		TEST_EQ(circ_buffer_is_full(ptb), true);

		// check that insertion fails (because full)
		TEST_EQ(circ_buffer_insert(ptb, NULL), false);

		for(int i = 0; i < NUM_ENTRIES - 1; ++i) {
			work_package taken_wp;
			TEST_EQ(circ_buffer_take(ptb, &taken_wp), true);

			TEST_EQ(taken_wp.timestamp, i);
			TEST_EQ(strcmp(taken_wp.entry, ""), 0);
		}
	}
}

