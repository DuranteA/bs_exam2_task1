#include "utils.h"

void millisleep(unsigned long duration) {
	struct timespec req;
	req.tv_sec = duration / 1000;
	req.tv_nsec = (duration % 1000) * 1000 * 1000;
	nanosleep(&req, NULL);
}
